
from http.client import NON_AUTHORITATIVE_INFORMATION
import string
from fastapi import APIRouter
from pydantic import BaseModel

router = APIRouter(
    prefix="/predict",
    tags=["predict"],
    responses={404: {"message": "Not found"}}
)
from facenet_pytorch import fixed_image_standardization, MTCNN, InceptionResnetV1
import glob
from torch.utils.data import SubsetRandomSampler
from PIL import Image, ImageDraw,ImageFont
import torch
import numpy as np
from torchvision import datasets, transforms
import joblib
import warnings
from sklearn.model_selection import RandomizedSearchCV, GridSearchCV
from sklearn.svm import SVC
import os 
os.chdir('C:/sas/facenet07')

ABS_PATH = '/sas/facenet07'
DATA_PATH = os.path.join(ABS_PATH, 'data')


import  joblib
SVM_PATH = os.path.join(DATA_PATH, 'svm.sav')
# joblib.dump(clf, SVM_PATH)
clf = joblib.load(SVM_PATH)


from torchvision import datasets, transforms


import numpy as np

# import shutil

os.system('pip install facenet-pytorch ')
from facenet_pytorch import fixed_image_standardization , MTCNN , InceptionResnetV1



def diag(x1, y1, x2, y2):
    return np.linalg.norm([x2 - x1, y2 - y1])


def square(x1, y1, x2, y2):
    return abs(x2 - x1) * abs(y2 - y1)


def isOverlap(rect1, rect2):
    x1, x2 = rect1[0], rect1[2]
    y1, y2 = rect1[1], rect1[3]

    x1_, x2_ = rect2[0], rect2[2]
    y1_, y2_ = rect2[1], rect2[3]

    if x1 > x2_ or x2 < x1_: return False 
    if y1 > y2_ or y2 < y1_: return False
  
    rght, lft = x1 < x1_ < x2, x1_ < x1 < x2_
    d1, d2 = 0, diag(x1_, y1_, x2_, y2_)
    threshold = 0.5

    if rght and y1 < y1_: d1 = diag(x1_, y1_, x2, y2)
    elif rght and y1 > y1_: d1 = diag(x1_, y2_, x2, y1)
    elif lft and y1 < y1_: d1 = diag(x2_, y1_, x1, y2) 
    elif lft and y1 > y1_: d1 = diag(x2_, y2_, x1, y1)

    if d1 / d2 >= threshold and square(x1, y1, x2, y2) < square(x1_, y1_, x2_, y2_): return True
    return False

def draw_box(draw, boxes, names, probs, min_p=0.89):
    font = ImageFont.truetype(os.path.join(ABS_PATH, 'arial.ttf'), size=22)

    not_overlap_inds = []
    for i in range(len(boxes)): 
        not_overlap = True
        for box2 in boxes: 
            if np.all(boxes[i] == box2): continue 
            not_overlap = not isOverlap(boxes[i], box2)   
            if not not_overlap: break 
        if not_overlap: not_overlap_inds.append(i)

    boxes = [boxes[i] for i in not_overlap_inds] 
    probs = [probs[i] for i in not_overlap_inds]
    for box, name, prob in zip(boxes, names, probs):
        if prob >= min_p: 
            draw.rectangle(box.tolist(), outline=(255, 255, 255), width=5)
            x1, y1, _, _ = box
            text_width, text_height = font.getsize(f'{name}')
            draw.rectangle(((x1, y1 - text_height), (x1 + text_width, y1)), fill='white')
            draw.text((x1, y1 - text_height), f'{name}: {prob:.2f}', (24, 12, 30), font) 
            
    return boxes, probs 


standard_transform = transforms.Compose([
                                transforms.Resize((160, 160)),
                                np.float32, 
                                transforms.ToTensor(),
                                fixed_image_standardization
])

def get_video_embedding(model, x): 
    embeds = model(x.to(device))
    return embeds.detach().cpu().numpy()

def face_extract(model, clf, frame, boxes):
    names, prob = [], []
    if len(boxes):
        x = torch.stack([standard_transform(frame.crop(b)) for b in boxes])
        embeds = get_video_embedding(model, x)
        idx, prob = clf.predict(embeds), clf.predict_proba(embeds).max(axis=1)
        names = [IDX_TO_CLASS[idx_] for idx_ in idx]
    return names, prob 

def preprocess_image(detector, face_extractor, clf, path, transform=None):
    if not transform: transform = lambda x: x.resize((1280, 1280)) if (np.array(x.size) > 2000).all() else x
    capture = Image.open(path).convert('RGB')
    i = 0 
    
    # iframe = Image.fromarray(transform(np.array(capture)))
    iframe = transform(capture)
   
    boxes, probs = detector.detect(iframe)
    if boxes is None: boxes, probs = [], []
    names, prob = face_extract(face_extractor, clf, iframe, boxes)
            
    frame_draw = iframe.copy()
    draw = ImageDraw.Draw(frame_draw)

    boxes, probs = draw_box(draw, boxes, names, probs)
    return frame_draw.resize((620, 480), Image.BILINEAR)


from PIL import ImageFont
import torch
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
k = 3 # each k image will be processed by networks
font = ImageFont.truetype(os.path.join(ABS_PATH, 'arial.ttf'), size=16)

mtcnn = MTCNN(keep_all=True, min_face_size=70, device=device)
model = InceptionResnetV1(pretrained='vggface2', dropout_prob=0.6, device=device).eval()



# **Some photos**

from PIL import Image, ImageDraw
from torch.utils.data import SubsetRandomSampler

b = 32
ABS_PATH = '/sas/facenet07'
DATA_PATH = os.path.join(ABS_PATH, 'data')
 
TRAIN_DIR = os.path.join(DATA_PATH, 'train_images')
ALIGNED_TRAIN_DIR = TRAIN_DIR + '_cropped'
trainD = datasets.ImageFolder(ALIGNED_TRAIN_DIR, transform=standard_transform)

IDX_TO_CLASS = np.array(list(trainD.class_to_idx.keys()))
CLASS_TO_IDX = dict(trainD.class_to_idx.items())


TRAIN_EMBEDS = os.path.join(DATA_PATH, 'trainEmbeds.npz')
TEST_EMBEDS = os.path.join(DATA_PATH, 'testEmbeds.npz')

# np.savez(TRAIN_EMBEDS, x=trainEmbeds, y=trainLabels)
# np.savez(TEST_EMBEDS, x=testEmbeds, y=testLabels)

# Load the saved embeddings to use them futher 
trainEmbeds, trainLabels = np.load(TRAIN_EMBEDS, allow_pickle=True).values()
testEmbeds, testLabels = np.load(TEST_EMBEDS, allow_pickle=True).values()

# Get named labels
trainLabels, testLabels = IDX_TO_CLASS[trainLabels], IDX_TO_CLASS[testLabels]

# data preparation 
X = np.copy(trainEmbeds)
y = np.array([CLASS_TO_IDX[label] for label in trainLabels])

print(f'X train embeds size: {X.shape}')
print(f'Tagret train size: {y.shape}')

# test data preparation 
X_test, y_test = np.copy(testEmbeds), np.array([CLASS_TO_IDX[label] for label in testLabels])
print(f'X train embeds size: {X_test.shape}')
print(f'Tagret train size: {y_test.shape}')


from sklearn.metrics import accuracy_score ,recall_score ,classification_report
import matplotlib.pyplot as plt
from sklearn.datasets import make_classification
from sklearn.metrics import ConfusionMatrixDisplay,confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
inds = range(88)
train_acc = accuracy_score(clf.predict(X[inds]), y[inds])
print(f'Accuracy score on train data: {train_acc:.3f}')

test_acc = accuracy_score(clf.predict(X_test), y_test)
print(f'Accuracy score on test data: {test_acc}')

# y_true =  y[inds]
y_pred = clf.predict(X[inds])
# train_recall = recall_score(y[inds], clf.predict(X[inds]), average=None)
# print(f'recall score on train data: {train_recall}')

test_recall = recall_score(y_test, clf.predict(X_test), average=None)
test_recall_result = sum(test_recall)/len(test_recall)
print(f'recall score on test data: {test_recall_result}')
predictions = clf.predict(X_test)
cm = confusion_matrix(y_test, predictions, labels=clf.classes_)
print(cm)
print(classification_report(y_test, predictions))



def get_files(path):
    imgs = []
    valid_images = [".jpg",".png",".jpeg"]
    for f in os.listdir(path):
        ext = os.path.splitext(f)[1]
        if ext.lower() not in valid_images:
            continue
        imgs.append(f)
    return imgs

def get_predict(courseOfferedIdPeriod):
    ADD_DATA = os.path.join(DATA_PATH, 'images')
    PREDICT_DIR = os.path.join(ADD_DATA,courseOfferedIdPeriod)
    predict = get_files(PREDICT_DIR)
    for p in predict:
        frame = preprocess_image(mtcnn, model, clf, os.path.join(PREDICT_DIR, p))
        frame.save(os.path.join(PREDICT_DIR, p+'predict'), 'png')
    
    
@router.get("/{courseOfferedIdPeriod}")
async def get_data(courseOfferedIdPeriod: str):
    return get_predict(courseOfferedIdPeriod)

