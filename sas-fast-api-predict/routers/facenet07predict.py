from http.client import NON_AUTHORITATIVE_INFORMATION
import string
from fastapi import APIRouter
from pydantic import BaseModel

router = APIRouter(
    prefix="/predict",
    tags=["predict"],
    responses={404: {"message": "Not found"}}
)

from facenet_pytorch import fixed_image_standardization, MTCNN, InceptionResnetV1
import glob
from torch.utils.data import SubsetRandomSampler
from PIL import Image, ImageDraw
import torch
import numpy as np
from torchvision import datasets, transforms
import joblib
import warnings
from sklearn.model_selection import RandomizedSearchCV, GridSearchCV
from sklearn.svm import SVC
import os

os.chdir('C:/sas/facenet07')

ABS_PATH = '/sas/facenet07'
DATA_PATH = os.path.join(ABS_PATH, 'data')


standard_transform = transforms.Compose([
    transforms.Resize((160, 160)),
    np.float32,
    transforms.ToTensor(),
    fixed_image_standardization
])

b = 32
ABS_PATH = '/sas/facenet07'
DATA_PATH = os.path.join(ABS_PATH, 'data')

TRAIN_DIR = os.path.join(DATA_PATH, 'train_images')
ALIGNED_TRAIN_DIR = TRAIN_DIR + '_cropped'
trainD = datasets.ImageFolder(ALIGNED_TRAIN_DIR, transform=standard_transform)

IDX_TO_CLASS = np.array(list(trainD.class_to_idx.keys()))
CLASS_TO_IDX = dict(trainD.class_to_idx.items())


TRAIN_EMBEDS = os.path.join(DATA_PATH, 'trainEmbeds.npz')

# Load the saved embeddings to use them futher
trainEmbeds, trainLabels = np.load(TRAIN_EMBEDS, allow_pickle=True).values()

# Get named labels
trainLabels = IDX_TO_CLASS[trainLabels]
# data preparation
X = np.copy(trainEmbeds)
y = np.array([CLASS_TO_IDX[label] for label in trainLabels])

warnings.filterwarnings('ignore', 'Solver terminated early.*')
param_grid = {'C': [1, 10, 100, 1e3, 5e3, 1e4, 5e4, 1e5], 'gamma': [
    0.0001, 0.0005, 0.001, 0.005, 0.01, 0.1, 'auto'], 'kernel': ['rbf', 'sigmoid', 'poly']}
model_params = {'class_weight': 'balanced',
                'max_iter': 10, 'probability': True, 'random_state': 3}
model = SVC(**model_params)
clf = GridSearchCV(model, param_grid)
# print('Best estimator: ', clf.best_estimator_)
# print('Best params: ', clf.best_params_)
clf = GridSearchCV(model, param_grid)
clf.fit(X, y)
SVM_PATH = os.path.join(DATA_PATH, 'svm.sav')
joblib.dump(clf, SVM_PATH)
clf = joblib.load(SVM_PATH)


# import shutil
os.system('pip install facenet-pytorch ')




def diag(x1, y1, x2, y2):
    return np.linalg.norm([x2 - x1, y2 - y1])


def square(x1, y1, x2, y2):
    return abs(x2 - x1) * abs(y2 - y1)


def isOverlap(rect1, rect2):
    x1, x2 = rect1[0], rect1[2]
    y1, y2 = rect1[1], rect1[3]

    x1_, x2_ = rect2[0], rect2[2]
    y1_, y2_ = rect2[1], rect2[3]

    if x1 > x2_ or x2 < x1_:
        return False
    if y1 > y2_ or y2 < y1_:
        return False

    rght, lft = x1 < x1_ < x2, x1_ < x1 < x2_
    d1, d2 = 0, diag(x1_, y1_, x2_, y2_)
    threshold = 0.5

    if rght and y1 < y1_:
        d1 = diag(x1_, y1_, x2, y2)
    elif rght and y1 > y1_:
        d1 = diag(x1_, y2_, x2, y1)
    elif lft and y1 < y1_:
        d1 = diag(x2_, y1_, x1, y2)
    elif lft and y1 > y1_:
        d1 = diag(x2_, y2_, x1, y1)

    if d1 / d2 >= threshold and square(x1, y1, x2, y2) < square(x1_, y1_, x2_, y2_):
        return True
    return False


def get_video_embedding(model, x):
    embeds = model(x.to(device))
    return embeds.detach().cpu().numpy()


def face_extract(model, clf, frame, boxes):
    names, prob = [], []
    if len(boxes):
        x = torch.stack([standard_transform(frame.crop(b)) for b in boxes])
        embeds = get_video_embedding(model, x)
        idx, prob = clf.predict(embeds), clf.predict_proba(embeds).max(axis=1)
        names = [IDX_TO_CLASS[idx_] for idx_ in idx]
    return names, prob

def preprocess_image(detector, face_extractor, clf, path, transform=None):
    if not transform: transform = lambda x: x.resize((1280, 1280)) if (np.array(x.size) > 2000).all() else x
    capture = Image.open(path).convert('RGB')
    # i = 0 
    
    # iframe = Image.fromarray(transform(np.array(capture)))
    iframe = transform(capture)
   
    boxes, probs = detector.detect(iframe)
    # if boxes is None: boxes, probs = [], []
    names, prob = face_extract(face_extractor, clf, iframe, boxes)
            
    frame_draw = iframe.copy()
    draw = ImageDraw.Draw(frame_draw)

    # boxes, probs = draw_box(draw, boxes, names, probs)
    # return frame_draw.resize((620, 480), Image.BILINEAR)
    return names,prob
# from PIL import ImageFont
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
k = 3  # each k image will be processed by networks
# font = ImageFont.truetype(os.path.join(ABS_PATH, 'arial.ttf'), size=16)

mtcnn = MTCNN(keep_all=True, min_face_size=70, device=device)
model = InceptionResnetV1(pretrained='vggface2',dropout_prob=0.6, device=device).eval()

# **Some photos**
from collections import Counter 

def get_files(path):
    imgs = []
    valid_images = [".jpg",".png",".jpeg"]
    for f in os.listdir(path):
        ext = os.path.splitext(f)[1]
        if ext.lower() not in valid_images:
            continue
        imgs.append(f)
    return imgs

@router.get("/{courseOfferedIdPeriod}")
async def get_data(courseOfferedIdPeriod: str):
    return get_predict(courseOfferedIdPeriod)


def get_predict(courseOfferedIdPeriod):
    ADD_DATA = os.path.join(DATA_PATH, 'images')
    PREDICT_DIR = os.path.join(ADD_DATA,courseOfferedIdPeriod)
    predict = get_files(PREDICT_DIR)
    # predict = Counter(map(os.path.dirname, predictF))
    nameAll = []
    prob = []
    data = []
    print(predict)
    for p in predict:
        nameAll, prob = preprocess_image(mtcnn, model, clf, os.path.join(PREDICT_DIR, p))
        
    # nameAll = names
    # prob = prob
    i = 0
    for x in nameAll:
        data.append(x)
        data.append(prob[i])
        i += 1
    print(prob)
    print(data)
    return  nameAll

