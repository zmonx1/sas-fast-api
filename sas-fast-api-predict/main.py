from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel
from routers import facenet07predict
# from routers import facenet03Prediction


import os 

app = FastAPI()

origins = [
    "http://127.0.0.1:8000/",
    "http://localhost/",
    "http://localhost:8080/",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
app.include_router(facenet07predict.router)

# @app.get("/train")
# async def root():
#     os.system('ipython facenet07.py')
#     return {"success"}
